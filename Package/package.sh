#!/bin/bash

set -x
set -e

helpFunction()
{
   echo ""
   echo "Usage: $0 -v version -d dockerfile"
   echo -e "\t-v The version info of deb package"
   echo -e "\t-d Dockerfile"
   exit 1 # Exit script after printing help
}

while getopts "v:d:" opt
do
   case "$opt" in
      v ) version="$OPTARG" ;;
      d ) dockerfile="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$version" ] || [ -z "$dockerfile" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

# Begin script in case all parameters are correct

current_path="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source_path=${current_path}/../
tmp_app_path=${current_path}/tmp
output_path=${current_path}/output

echo "Version: $version"
echo "Building docker file"
docker build --build-arg VERSION_INFO=$version -t hawkbit . -f $dockerfile
echo "Running docker container"
docker stop hawkbit || true
docker rm hawkbit || true
docker run -v ${source_path}:/source --name hawkbit -d hawkbit
echo "Showing logs on container"
docker logs -f hawkbit

